﻿namespace = anb_religious_decision

# Corset Burials
anb_religious_decision.0001 = {
	type = character_event
	title = anb_religious_decision.0001.t
	desc = anb_religious_decision.0001.desc
	theme = faith
	override_background = {
		reference = wilderness 
	}

	left_portrait = {
		character = var:ancestor_to_bury
	}

	immediate = {
		add_piety = major_piety_value
		if = {
			limit = {
				any_vassal = {
					faith = {
						has_doctrine_parameter = corset_burials_active
					}
				}
			}
			every_vassal = {
				limit = {
					faith = {
						has_doctrine_parameter = corset_burials_active
					}
				}
				custom = give_sky_burial_vassals
				add_opinion = {
					modifier = pleased_opinion
					target = root
					opinion = 20
				}
			}
		}
	}

	option = {
		name = anb_religious_decision.0001.a
	}

	after = {
		remove_variable = ancestor_to_bury
	}
}

# Child of Jaher's line gains proper trait (fired on on_birth).
anb_religious_decision.0002 = {
	type = character_event
	hidden = yes
	
	trigger = {
		OR = {
			AND = {
				exists = mother
				mother = {
					OR = {
						has_trait = sun_reborn
						has_trait = sun_reborn_descendant
					}
				}
			}
			AND = {
				exists = father
				father = {
					OR = {
						has_trait = sun_reborn
						has_trait = sun_reborn_descendant
					}
				}
			}
		}
		NOR = {
			has_trait = sun_reborn # Just in case the tree was horribly tangled...
			has_trait = sun_reborn_descendant
		}
	}
	
	immediate = {
		add_trait = sun_reborn_descendant
		every_child = {
			trigger_event = anb_religious_decision.0002
		}
	}
}

#Skaldhyrric

anb_religious_decision.0501 = {
	type = character_event
	title = anb_religious_decision.0501.t
	desc = {
		desc = anb_religious_decision.0501.desc.beginning
	}
	theme = faith
	override_background = {
		reference = fp1_tribal_temple 
	}
	left_portrait = {
		character = root.realm_priest
		animation = personality_zealous
	}
	
	right_portrait = root

	# TODO - AI Chances

	option = { #Let Skald do it
		name = anb_religious_decision.0501.a

		trigger_event = anb_religious_decision.0502
	}
	option = { #Do it yourself
		name = anb_religious_decision.0501.b
		
		trigger_event = anb_religious_decision.0503
	}
	option = { #Don't do it
		name = anb_religious_decision.0501.c
		
		add_piety = 100
		remove_variable = reciting_skaldic_tales
	}
}

anb_religious_decision.0502 = { #Let Skald do it
	type = character_event
	title = anb_religious_decision.0502.t
	desc = {
		desc = anb_religious_decision.0502.desc.beginning
	}
	theme = faith
	override_background = {
		reference = fp1_tribal_temple 
	}
	left_portrait = {
		character = root.realm_priest
		animation = personality_zealous
	}

	after = {
		remove_variable = reciting_skaldic_tales
	}
	
	right_portrait = root

	option = { #Dirge of the Deep
		name = anb_religious_decision.0502.a
		
		add_piety = miniscule_piety_loss
		add_character_modifier = {
			modifier = skaldhyrric_dirge
			days = 5475
		}
	}
	option = { #Gjalund and the Giantslayers
		name = anb_religious_decision.0502.b
		
		add_piety = minor_piety_loss
		add_character_modifier = {
			modifier = skaldhyrric_gjalund
			days = 5475
		}
	}
	option = { #Beralic Saga
		name = anb_religious_decision.0502.c
		
		add_piety = minor_piety_loss
		add_character_modifier = {
			modifier = skaldhyrric_beralic
			days = 5475
		}
	}
	option = { #Voyage of the North Raider
		name = anb_religious_decision.0502.d
		
		add_piety = medium_piety_loss
		add_character_modifier = {
			modifier = skaldhyrric_voyage
			days = 5475
		}
	}
	option = { #Treasure of the Golden Forest
		name = anb_religious_decision.0502.e
		
		add_piety = medium_piety_loss
		add_character_modifier = {
			modifier = skaldhyrric_golden_forest
			days = 5475
		}
	}
	option = { #Master Boat Builders
		name = anb_religious_decision.0502.f
		
		add_piety = major_piety_loss
		add_character_modifier = {
			modifier = skaldhyrric_master_boatbuilders
			days = 5475
		}
	}
	option = { #Dragon and the Skald
		name = anb_religious_decision.0502.g
		
		add_piety = major_piety_loss
		add_character_modifier = {
			modifier = skaldhyrric_dragon_and_skald
			days = 5475
		}
	}
	option = { #Old Winter Lullaby
		name = anb_religious_decision.0502.h
		
		add_piety = major_piety_loss
		add_character_modifier = {
			modifier = skaldhyrric_old_winter_lullaby
			days = 5475
		}
	}
	option = { #Don't do it
		name = anb_religious_decision.0501.c
		
		add_piety = 100
	}
}

anb_religious_decision.0503 = { #Do it yourself
	type = character_event
	title = anb_religious_decision.0503.t
	desc = {
		desc = anb_religious_decision.0503.desc.beginning
	}
	theme = faith
	override_background = {
		reference = fp1_tribal_temple 
	}
	left_portrait = {
		character = root.realm_priest
		animation = personality_zealous
	}
	
	after = {
		remove_variable = reciting_skaldic_tales
	}

	right_portrait = root

	option = { #Dirge of the Deep
		name = anb_religious_decision.0503.a
		
		duel = {
			skill = learning
			value = mediocre_skill_rating 
			60 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = 3
				}
				desc = anb_religious_decision.0503.a.success
				add_character_modifier = {
					modifier = skaldhyrric_dirge
					days = 5475
				}
				add_prestige = miniscule_prestige_gain
			}
			40 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = -2
				}
				desc = anb_religious_decision.0503.failure
				add_prestige = miniscule_prestige_loss
			}
		}
	}
	option = { #Gjalund and the Giantslayers
		name = anb_religious_decision.0503.b
		
		duel = {
			skill = learning
			value = medium_skill_rating
			60 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = 3
				}
				desc = anb_religious_decision.0503.b.success
				add_character_modifier = {
					modifier = skaldhyrric_gjalund
					days = 5475
				}
				add_prestige = minor_prestige_gain
			}
			40 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = -2
				}
				desc = anb_religious_decision.0503.failure
				add_prestige = minor_prestige_loss
			}
		}
	}
	option = { #Beralic Saga
		name = anb_religious_decision.0503.c
		
		duel = {
			skill = learning
			value = medium_skill_rating
			60 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = 3
				}
				desc = anb_religious_decision.0503.c.success
				add_character_modifier = {
					modifier = skaldhyrric_beralic
					days = 5475
				}
				add_prestige = minor_prestige_gain
			}
			40 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = -2
				}
				desc = anb_religious_decision.0503.failure
				add_prestige = minor_prestige_loss
			}
		}
	}
	option = { #Voyage of the North Raider
		name = anb_religious_decision.0503.d
		
		duel = {
			skill = learning
			value = high_skill_rating
			60 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = 3
				}
				desc = anb_religious_decision.0503.d.success
				add_character_modifier = {
					modifier = skaldhyrric_voyage
					days = 5475
				}
				add_prestige = medium_prestige_gain
			}
			40 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = -2
				}
				desc = anb_religious_decision.0503.failure
				add_prestige = medium_prestige_loss
			}
		}
	}
	option = { #Treasure of the Golden Forest
		name = anb_religious_decision.0503.e
		
		duel = {
			skill = learning
			value = high_skill_rating
			60 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = 3
				}
				desc = anb_religious_decision.0503.e.success
				add_character_modifier = {
					modifier = skaldhyrric_golden_forest
					days = 5475
				}
				add_prestige = medium_prestige_gain
			}
			40 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = -2
				}
				desc = anb_religious_decision.0503.failure
				add_prestige = medium_prestige_loss
			}
		}
	}
	option = { #Master Boat Builders
		name = anb_religious_decision.0503.f
		
		duel = {
			skill = learning
			value = very_high_skill_rating
			60 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = 3
				}
				desc = anb_religious_decision.0503.f.success
				add_character_modifier = {
					modifier = skaldhyrric_master_boatbuilders
					days = 5475
				}
				add_prestige = major_prestige_gain
			}
			40 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = -2
				}
				desc = anb_religious_decision.0503.failure
				add_prestige = major_prestige_loss
			}
		}
	}
	option = { #Dragon and the Skald
		name = anb_religious_decision.0503.g
		
		duel = {
			skill = learning
			value = extremely_high_skill_rating
			60 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = 3
				}
				desc = anb_religious_decision.0503.g.success
				add_character_modifier = {
					modifier = skaldhyrric_dragon_and_skald
					days = 5475
				}
				add_prestige = massive_prestige_gain
			}
			40 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = -2
				}
				desc = anb_religious_decision.0503.failure
				add_prestige = massive_prestige_loss
			}
		}
	}
	option = { #Old Winter Lullaby
		name = anb_religious_decision.0503.h
		
		duel = {
			skill = learning
			value = extremely_high_skill_rating
			60 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = 3
				}
				desc = anb_religious_decision.0503.h.success
				add_character_modifier = {
					modifier = skaldhyrric_old_winter_lullaby
					days = 5475
				}
				add_prestige = massive_prestige_gain
			}
			40 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = -2
				}
				desc = anb_religious_decision.0503.failure
				add_prestige = massive_prestige_loss
			}
		}
	}
	option = { #Don't do it
		name = anb_religious_decision.0501.c
		
		add_piety = 100
	}
}
anb_religious_decision.0104 = {
	type = character_event
	title = anb_religious_decision.0104.t
	desc = anb_religious_decision.0104.desc
	theme = faith
	left_portrait = {
		character = root
		animation = personality_rational
	}

	# Artanos
	option = {
		name = religious_decision.01.artanos
		trigger = {
			NOT = { has_character_modifier = artanos_deity }
		}
		set_bhakti_effect = { BHAKTI = artanos_deity }
	}
	option = {
		name = religious_decision.01.merisse
		trigger = {
			NOT = { has_character_modifier = merisse_deity }
		}
		set_bhakti_effect = { BHAKTI = merisse_deity }
	}
	option = {
		name = religious_decision.01.trovecos
		trigger = {
			NOT = { has_character_modifier = trovecos_deity }
		}
		set_bhakti_effect = { BHAKTI = trovecos_deity }
	}
	option = {
		name = religious_decision.01.careslobos
		trigger = {
			NOT = { has_character_modifier = careslobos_deity }
		}
		set_bhakti_effect = { BHAKTI = careslobos_deity }
	}
	option = {
		name = religious_decision.01.asmirethin
		trigger = {
			NOT = { has_character_modifier = asmirethin_deity }
		}
		set_bhakti_effect = { BHAKTI = asmirethin_deity }
	}
	option = {
		name = religious_decision.01.more_gods
		trigger_event = anb_religious_decision.0204
	}

}

anb_religious_decision.0204 = {
	type = character_event
	title = anb_religious_decision.0204.t
	desc = anb_religious_decision.0204.desc
	theme = faith
	left_portrait = {
		character = root
		animation = personality_rational
	}

	option = {
		name = religious_decision.02.damarta
		trigger = {
			NOT = { has_character_modifier = damarta_deity }
		}
		set_bhakti_effect = { BHAKTI = damarta_deity }
	}
	option = {
		name = religious_decision.02.belouina
		trigger = {
			NOT = { has_character_modifier = belouina_deity }
		}
		set_bhakti_effect = { BHAKTI = belouina_deity }
	}
	option = {
		name = religious_decision.02.dercanos
		trigger = {
			NOT = { has_character_modifier = dercanos_deity }
		}
		set_bhakti_effect = { BHAKTI = dercanos_deity }
	}
	option = {
		name = religious_decision.02.sorbodua
		trigger = {
			NOT = { has_character_modifier = sorbodua_deity }
		}
		set_bhakti_effect = { BHAKTI = sorbodua_deity }
		
	}
	option = {
		name = religious_decision.02.turanos
		trigger = {
			NOT = { has_character_modifier = turanos_deity }
		}
		set_bhakti_effect = { BHAKTI = turanos_deity }

	}
	option = {
		name = religious_decision.02.more_gods
		trigger_event = anb_religious_decision.0104
	}
}

anb_religious_decision.0301 = {
	type = character_event
	title = anb_religious_decision.0301.t
	desc = anb_religious_decision.0301.desc
	theme = faith
	left_portrait = {
		character = root
		animation = personality_rational
	}

	option = {
		name = religious_decision.01.edurunigal
		trigger = {
			NOT = { has_character_modifier = edurunigal_deity }
		}
		set_bhakti_effect = { BHAKTI = edurunigal_deity }
	}
	option = {
		name = religious_decision.01.nimensar
		trigger = {
			NOT = { has_character_modifier = nimensar_deity }
		}
		set_bhakti_effect = { BHAKTI = nimensar_deity }
	}
	option = {
		name = religious_decision.01.ninu
		trigger = {
			NOT = { has_character_modifier = ninu_deity }
		}
		set_bhakti_effect = { BHAKTI = ninu_deity }
	}
	option = {
		name = religious_decision.01.suhus
		trigger = {
			NOT = { has_character_modifier = suhus_deity }
		}
		set_bhakti_effect = { BHAKTI = suhus_deity }
	}
	option = {
		name = religious_decision.01.zamagur
		trigger = {
			NOT = { has_character_modifier = zamagur_deity }
		}
		set_bhakti_effect = { BHAKTI = zamagur_deity }
	}
	option = {
		name = anb_bulwar_religious_decision.01.more_gods
		trigger_event = anb_religious_decision.0302
	}
}

anb_religious_decision.0302 = {
	type = character_event
	title = anb_religious_decision.0302.t
	desc = anb_religious_decision.0302.desc
	theme = faith
	left_portrait = {
		character = root
		animation = personality_rational
	}

	option = {
		name = religious_decision.02.dumegir
		trigger = {
			NOT = { has_character_modifier = dumegir_deity }
		}
		set_bhakti_effect = { BHAKTI = dumegir_deity }
	}
	option = {
		name = religious_decision.02.gelsibir
		trigger = {
			NOT = { has_character_modifier = gelsibir_deity }
		}
		set_bhakti_effect = { BHAKTI = gelsibir_deity }
	}
	option = {
		name = religious_decision.02.tambora
		trigger = {
			NOT = { has_character_modifier = tambora_deity }
		}
		set_bhakti_effect = { BHAKTI = tambora_deity }
	}
	option = {
		name = religious_decision.02.kirasi
		trigger = {
			NOT = { has_character_modifier = kirasi_deity }
		}
		set_bhakti_effect = { BHAKTI = kirasi_deity }
	}
	option = {
		name = religious_decision.02.gikud
		trigger = {
			NOT = { has_character_modifier = gikud_deity }
		}
		set_bhakti_effect = { BHAKTI = gikud_deity }
	}
	option = {
		name = anb_bulwar_religious_decision.02.more_gods
		trigger_event = anb_religious_decision.0303
	}
}

anb_religious_decision.0303 = {
	type = character_event
	title = anb_religious_decision.0303.t
	desc = anb_religious_decision.0303.desc
	theme = faith
	left_portrait = {
		character = root
		animation = personality_rational
	}

	option = {
		name = religious_decision.03.genna
		trigger = {
			NOT = { has_character_modifier = genna_deity }
		}
		set_bhakti_effect = { BHAKTI = genna_deity }
	}
	option = {
		name = religious_decision.03.pinnagar
		trigger = {
			NOT = { has_character_modifier = pinnagar_deity }
		}
		set_bhakti_effect = { BHAKTI = pinnagar_deity }
	}
	option = {
		name = religious_decision.03.sidim
		trigger = {
			NOT = { has_character_modifier = sidim_deity }
		}
		set_bhakti_effect = { BHAKTI = sidim_deity }
	}
	option = {
		name = anb_bulwar_religious_decision.03.more_gods
		trigger_event = anb_religious_decision.0301
	}
}

anb_religious_decision.0304 = {
	type = character_event
	title = anb_religious_decision.0304.t
	desc = anb_religious_decision.0304.desc
	theme = faith
	left_portrait = {
		character = root
		animation = personality_rational
	}

	option = {
		name = religious_decision.01.sarnagir
		trigger = {
			NOT = { has_character_modifier = sarnagir_deity }
		}
		set_bhakti_effect = { BHAKTI = sarnagir_deity }
	}
	option = {
		name = religious_decision.02.lahmas
		trigger = {
			NOT = { has_character_modifier = lahmas_deity }
		}
		set_bhakti_effect = { BHAKTI = lahmas_deity }
	}
	option = {
		name = religious_decision.03.amasuri
		trigger = {
			NOT = { has_character_modifier = amasuri_deity }
		}
		set_bhakti_effect = { BHAKTI = amasuri_deity }
	}
	option = {
		name = religious_decision.04.brasan
		trigger = {
			NOT = { has_character_modifier = brasan_deity }
		}
		set_bhakti_effect = { BHAKTI = brasan_deity }
	}
}

anb_religious_decision.0305 = {
	type = character_event
	title = anb_religious_decision.0305.t
	desc = anb_religious_decision.0305.desc
	theme = faith
	left_portrait = {
		character = root
		animation = personality_rational
	}

	option = {
		name = religious_decision.01.allania
		trigger = {
			NOT = { has_character_modifier = allania_deity }
		}
		set_bhakti_effect = { BHAKTI = allania_deity }
	}
	option = {
		name = religious_decision.01.elizarian
		trigger = {
			NOT = { has_character_modifier = elizarian_deity }
		}
		set_bhakti_effect = { BHAKTI = elizarian_deity }
	}
	option = {
		name = religious_decision.01.jaherion
		trigger = {
			NOT = { has_character_modifier = jaherion_deity }
		}
		set_bhakti_effect = { BHAKTI = jaherion_deity }
	}
	option = {
		name = religious_decision.01.jaerelian
		trigger = {
			NOT = { has_character_modifier = jaerelian_deity }
		}
		set_bhakti_effect = { BHAKTI = jaerelian_deity }
	}
	option = {
		name = religious_decision.01.aldarien
		trigger = {
			NOT = { has_character_modifier = aldarien_deity }
		}
		set_bhakti_effect = { BHAKTI = aldarien_deity }
	}
	option = {
		name = anb_bulwar_religious_decision.04.more_gods
		trigger_event = anb_religious_decision.0306
	}
}

anb_religious_decision.0306 = {
	type = character_event
	title = anb_religious_decision.0306.t
	desc = anb_religious_decision.0306.desc
	theme = faith
	left_portrait = {
		character = root
		animation = personality_rational
	}

	option = {
		name = religious_decision.02.erelessa
		trigger = {
			NOT = { has_character_modifier = erelessa_deity }
		}
		set_bhakti_effect = { BHAKTI = erelessa_deity }
	}
	option = {
		name = religious_decision.02.velara
		trigger = {
			NOT = { has_character_modifier = velara_deity }
		}
		set_bhakti_effect = { BHAKTI = velara_deity }
	}
	option = {
		name = religious_decision.02.lezlindel
		trigger = {
			NOT = { has_character_modifier = lezlindel_deity }
		}
		set_bhakti_effect = { BHAKTI = lezlindel_deity }
	}
	option = {
		name = religious_decision.02.andrellion
		trigger = {
			NOT = { has_character_modifier = andrellion_deity }
		}
		set_bhakti_effect = { BHAKTI = andrellion_deity }
	}
	option = {
		name = religious_decision.02.jaddarion
		trigger = {
			NOT = { has_character_modifier = jaddarion_deity }
		}
		set_bhakti_effect = { BHAKTI = jaddarion_deity }
	}
	option = {
		name = anb_bulwar_religious_decision.05.more_gods
		trigger_event = anb_religious_decision.0307
	}
}

anb_religious_decision.0307 = {
	type = character_event
	title = anb_religious_decision.0307.t
	desc = anb_religious_decision.0307.desc
	theme = faith
	left_portrait = {
		character = root
		animation = personality_rational
	}

	option = {
		name = religious_decision.03.taelarian
		trigger = {
			NOT = { has_character_modifier = taelarian_deity }
		}
		set_bhakti_effect = { BHAKTI = taelarian_deity }
	}
	option = {
		name = religious_decision.03.daraztarion
		trigger = {
			NOT = { has_character_modifier = daraztarion_deity }
		}
		set_bhakti_effect = { BHAKTI = daraztarion_deity }
	}
	option = {
		name = anb_bulwar_religious_decision.06.more_gods
		trigger_event = anb_religious_decision.0305
	}
}