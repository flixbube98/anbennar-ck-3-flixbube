﻿namespace = anb_holy_orders

# Title Cleanup
# Saltmarcher Holdings
anb_holy_orders.1 = {
	type = character_event
	theme = war
	hidden = yes
	orphan = yes
	left_portrait = {
		character = root
		animation = personality_bold
	}
	
	immediate = {
		every_living_character = {
			limit = {
				any_held_title = {
					this = title:d_knights_of_the_saltmarch
				}
			}
			save_scope_as = leader
		}
	}
	
	option = {
		hidden_effect = {
			if = {
				limit = { NOT = { title:b_saltsfort.holder = scope:leader } }
				create_title_and_vassal_change = {
					type = leased_out
					save_scope_as = change
					add_claim_on_loss = no
				}
				title:b_saltsfort = {
					change_title_holder_include_vassals = {
						holder = scope:leader
						change = scope:change
					}
				}
				resolve_title_and_vassal_change = scope:change
			}
		}

		create_holy_order = {
			leader = scope:leader
			capital = title:b_saltsfort
			save_scope_as = saltmarcher_holy_order
		}
		
		set_global_variable = {
			name = saltmarcher_holy_order_title
			value = scope:saltmarcher_holy_order.title
		}

		hidden_effect = {
			scope:leader = {
				add_gold = 100 #So that they have some money to lend out
				add_piety_level = 2
				add_gold = holy_order_starting_gold
				every_courtier = {
					add_trait = order_member
				}
			}
		}
		global_var:saltmarcher_holy_order_title = {
			copy_title_history = title:d_knights_of_the_saltmarch
		}

		create_holy_order_effect = yes
	}
}