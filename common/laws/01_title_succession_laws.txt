﻿# Anbennar - added elven and Castnorian succession law

title_succession_laws = {
	#Default Feudal Elective
	feudal_elective_succession_law = {
		can_have = {
			government_has_flag = government_is_feudal
			highest_held_title_tier >= tier_duchy
			NOR = { #Cultures have their special flavor.
				culture = { has_cultural_parameter = witenagemot_succession_enabled }
				culture = { has_cultural_parameter = scandinavian_elective_enabled }
				culture = { has_cultural_parameter = tribal_elective_enabled } #FP3 addition
			}
		}
		can_pass = {
			can_change_title_law_trigger = yes
		}
		can_title_have = {
			can_title_have_law_general_trigger = yes
		}
		succession = {
			order_of_succession = election
			election_type = feudal_elective
		}
		flag = elective_succession_law
		title_allegiance_opinion = 10
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}

	#HRE Succession
	princely_elective_succession_law = {
		can_have = {
			OR = {
				government_has_flag = government_is_feudal
				government_has_flag = government_is_clan
			}
			highest_held_title_tier = tier_empire
		}
		can_pass = {
			can_change_title_law_trigger = yes
		}
		can_title_have = {
			# this = title:e_hre
			can_title_have_law_general_trigger = yes
			# Anbennar
			always = no
		}
		succession = {
			order_of_succession = election
			election_type = princely_elective
		}
		flag = elective_succession_law
		title_allegiance_opinion = 10
		modifier = {
			vassal_limit = 20
			minority_opinion = -15
		}
		revoke_cost = {
			prestige = change_hre_title_succession_law_prestige_cost
		}
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}

	#Witenagemot
	saxon_elective_succession_law = {
		can_have = {
			OR = {
				government_has_flag = government_is_feudal
				government_has_flag = government_is_clan
				government_has_flag = government_is_tribal
			}
			highest_held_title_tier >= tier_kingdom
		}
		can_pass = {
			can_change_title_law_trigger = yes
			culture = { has_cultural_parameter = witenagemot_succession_enabled }
		}
		can_title_have = {
			can_title_have_law_general_trigger = yes
		}
		succession = {
			order_of_succession = election
			election_type = saxon_elective
		}
		flag = elective_succession_law
		title_allegiance_opinion = 5
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}

	#Thing
	scandinavian_elective_succession_law = {
		can_have = {
			OR = {
				government_has_flag = government_is_feudal
				government_has_flag = government_is_clan
				government_has_flag = government_is_tribal
			}
			highest_held_title_tier >= tier_duchy
		}
		can_pass = {
			can_change_title_law_trigger = yes
			culture = { has_cultural_parameter = scandinavian_elective_enabled }
			#ANBENNAR TODO - prob add gerudian here if it makes sense
		}
		can_title_have = {
			can_title_have_law_general_trigger = yes
		}
		succession = {
			order_of_succession = election
			election_type = scandinavian_elective
		}
		flag = elective_succession_law
		title_allegiance_opinion = 5
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}

	#Tanistry
	gaelic_elective_succession_law = {
		can_have = {
			OR = {
				government_has_flag = government_is_feudal
				government_has_flag = government_is_clan
				government_has_flag = government_is_tribal
			}
			highest_held_title_tier >= tier_duchy
		}
		can_pass = {
			can_change_title_law_trigger = yes
			custom_description = {
				OR = {
					culture = { has_cultural_pillar = heritage_brythonic }
					culture = { has_cultural_pillar = heritage_goidelic }
				}
				text = succession_laws_must_have_valid_tanistry_culture
			}
		}
		can_title_have = {
			can_title_have_law_general_trigger = yes
		}
		succession = {
			order_of_succession = election
			election_type = gaelic_elective
		}
		flag = elective_succession_law
		title_allegiance_opinion = 5
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}

	#Jirga elective
	tribal_elective_succession_law = { #FOR FP3
		can_have = {
			OR = {
				government_has_flag = government_is_feudal
				government_has_flag = government_is_clan
				government_has_flag = government_is_tribal
			}
			highest_held_title_tier >= tier_kingdom
		}
		can_pass = {
			can_change_title_law_trigger = yes
			culture = { has_cultural_parameter = tribal_elective_enabled }
		}
		can_title_have = {
			can_title_have_law_general_trigger = yes
		}
		succession = {
			order_of_succession = election
			election_type = tribal_elective
		}
		flag = elective_succession_law
		title_allegiance_opinion = 5
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}

	#Player Heir
	temporal_head_of_faith_succession_law = {
		can_title_have = {
			is_temporal_head_of_faith_trigger = yes
		}
		should_show_for_title = { # Never show this in the UI, just apply it through script
			always = no
		}
		can_remove_from_title = {
			custom_description = {
				text = succession_laws_must_not_be_temporal
				is_temporal_head_of_faith_trigger = no
			}
		}
		succession = {
			order_of_succession = player_heir
		}
	}

	#Noble Family Succession
	noble_family_succession_law = {
		can_title_have = {
			is_noble_family_title = yes
		}
		should_show_for_title = { # Never show this in the UI, just apply it through script
			always = no
		}
		can_remove_from_title = {
			always = no
		}
		succession = {
			order_of_succession = noble_family
		}
		flag = can_designate_heirs
	}
	
	
	# Anbennar below here
	
	#ANBENNAR TODO - update this
	#Elven
	elven_elective_succession_law = {
		can_have = {
			OR = {
				has_government = feudal_government
				has_government = tribal_government
			}
			highest_held_title_tier >= tier_duchy
		}
		can_pass = {
			can_change_title_law_trigger = yes
			is_elvish_culture = yes
			is_race = { RACE = elf }
		}
		can_title_have = {
			can_title_have_law_general_trigger = yes
		}
		succession = {
			order_of_succession = election
			election_type = elven_elective
		}
		flag = elective_succession_law
		title_allegiance_opinion = 10
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	# Castanorian
	castanorian_ducal_elective_succession_law = {
		can_have = {
			OR = {
				has_government = feudal_government
				has_government = tribal_government
			}
			highest_held_title_tier >= tier_duchy # Can only set it if you own a duchy or above title to add it to
		}
		can_pass = {
			can_change_title_law_trigger = yes
		}
		can_title_have = {
			can_title_have_law_general_trigger = yes
			tier = tier_duchy
			# Only those below Castellyr, cast castonath, or anor
			OR = {
				kingdom = title:k_cast
				kingdom = title:k_anor
				kingdom = title:k_castonath
				kingdom = title:k_castellyr
			}
		}
		succession = {
			order_of_succession = election
			election_type = castanorian_ducal_elective
		}
		flag = elective_succession_law
		title_allegiance_opinion = 10
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
}
