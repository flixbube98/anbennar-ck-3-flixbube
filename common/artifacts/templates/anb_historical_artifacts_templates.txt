﻿
calindal_template = {
	can_equip = {
		has_character_modifier = wielded_calindal
	}
	# You can't leave it on a pedastal, it needs to be wielded!
	can_reforge = {
		always = no
	}
	ai_score = {
		value = 1000
	}

	unique = yes
}

dinatoldir_template = {
	# can this character benefit from the full modifiers of the artifact?
	can_benefit = {
		faith = {
			OR = {
				has_doctrine = tenet_suraels_rebirth
			}
		}
	}
	
	can_repair = {
		culture = {
			has_innovation = innovation_plate_armor # Advanced metalworking
		}
	}
	
	# You can't leave it on a pedastal, it needs to be wielded!
	can_reforge = {
		always = no
	}

	# if a given character does not pass the "can_benefit" trigger then this modifier will be applied instead.
	fallback = {
		monthly_prestige = 0.5
		monthly_dynasty_prestige = 0.04
		monthly_dynasty_prestige_mult = 0.05
		prowess = 10
		tolerance_advantage_mod = 8
	}
	
	ai_score = {
		value = 1000
	}

	unique = yes
}

ruby_crown_template = {
	# You can't leave it on a pedastal, it needs to be worn!
	can_reforge = {
		always = no
	}

	ai_score = {
		value = 1000
	}

	unique = yes
}

gem_of_asra_template = {
	# You can't leave it on a pedastal, it needs to be worn!
	can_reforge = {
		always = no
	}

	ai_score = {
		value = 1000
	}

	unique = yes
}

crest_of_ishtara_template = {
	# You can't leave it on a pedastal, it needs to be worn!
	can_reforge = {
		always = no
	}

	ai_score = {
		value = 1000
	}

	unique = yes
}

pearl_of_the_dame_template = {
	can_reforge = {
		always = no
	}

	ai_score = {
		value = 1000
	}

	unique = yes
}

swansong_template = {	
	can_benefit = {
		OR = {
			diplomacy >= 12
			has_trait = poet
		}
	}
	
	ai_score = {
		value = 1000
	}
	
	unique = yes
}

carillon_of_the_lunetein_template = {
	can_benefit = {
		faith = faith:elven_forebears
	}
	
	fallback = {
		monthly_dynasty_prestige = 0.05
		monthly_prestige = 0.6
		movement_speed = 0.1
		pursue_efficiency = 0.1
	}
	
	ai_score = {
		value = 1000
	}
	
	unique = yes
}

flag_of_the_diranhria_template = {
	can_benefit = {
		faith = faith:jaherian_cults
	}
	
	fallback = {
		monthly_prestige = 0.3
		naval_movement_speed_mult = 0.1
	}
	
	ai_score = {
		value = 1000
	}
	
	unique = yes
}

ebonsteel_sword_of_bjarnrik_template = {
	can_reforge = {
		always = no
	}
	
	ai_score = {
		value = 1000
	}

	unique = yes
}
