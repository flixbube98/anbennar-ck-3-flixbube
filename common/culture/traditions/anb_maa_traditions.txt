﻿# Anbennar: commenting vanilla stuff

#######################
# CULTURAL TRADITIONS #
#######################

######################
# MAA Traditions 	 #
######################

tradition_test_maa = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		always = no
	}
	can_pick = {
		always = no
	}
	
	parameters = {
		# archer
		unlock_maa_anb_dwarven_handgunner = yes
		unlock_maa_anb_longbowmen = yes
		unlock_maa_anb_oakfoot_rangers = yes
		unlock_maa_anb_troll_hunter = yes
		unlock_maa_anb_forest_stalkers = yes
		unlock_maa_anb_arbalesters = yes

		# skirmisher
		unlock_maa_anb_small_militia = yes
		unlock_maa_anb_woodsmen = yes
		unlock_maa_anb_agderbedderen = yes
		unlock_maa_anb_moormen_clan_raiders = yes
		unlock_maa_anb_alononan = yes
		unlock_maa_anb_saerines = yes
		unlock_maa_anb_mistfighters = yes
		unlock_maa_anb_vernmen_veterans = yes

		# heavy infantry
		unlock_maa_anb_dercinges = yes
		unlock_maa_anb_wexonard_greatswords = yes
		unlock_maa_anb_patrician_guards = yes
		unlock_maa_anb_ebonsteel_infantry = yes
		unlock_maa_anb_havoric_clansmen = yes
		unlock_maa_anb_vrorengards = yes
		unlock_maa_anb_balakuran = yes
		unlock_maa_anb_bladesworn = yes
		unlock_maa_anb_spadiores = yes
		unlock_maa_anb_taldoud_guard = yes
		unlock_maa_anb_arannese_bardiches = yes

		# pikemen
		unlock_maa_anb_mireknights = yes
		unlock_maa_anb_isargaesos = yes
		unlock_maa_anb_bulwari_phalanx = yes
		unlock_maa_anb_gawedi_talonmen = yes
		unlock_maa_anb_thorn_squares = yes
		unlock_maa_anb_femaor_warriors = yes
		unlock_maa_anb_burning_guard = yes
		unlock_maa_anb_ribbonspears = yes

		# mages
		unlock_maa_anb_battlemages = yes
		unlock_maa_anb_druids = yes
		unlock_maa_anb_skalds = yes
		unlock_maa_anb_storm_sorcerers = yes
		unlock_maa_anb_wren_hedge_witches = yes

		# light cavalry
		unlock_maa_anb_alenic_outriders = yes
		unlock_maa_anb_smallknights = yes
		unlock_maa_anb_epuires = yes
		unlock_maa_anb_businori_raiders = yes
		unlock_maa_anb_wayknights = yes
		unlock_maa_anb_knights_of_the_serpent = yes

		# heavy cavalry
		unlock_maa_anb_rose_knights = yes
		unlock_maa_anb_akalite_cataphracts = yes
		unlock_maa_anb_flying_chariots = yes
		unlock_maa_anb_knights_of_the_spear = yes
		unlock_maa_anb_castellyrian_heavy_knights = yes
		unlock_maa_anb_arbarani_heavy_riders = yes
		unlock_maa_anb_smallknights = yes

		# elephant cavalry
		unlock_maa_anb_war_mammoths = yes

		# beasts
		unlock_maa_anb_griffon_riders = yes
		unlock_maa_anb_wyvern_riders = yes
		unlock_maa_anb_drake_riders = yes
		unlock_maa_anb_salamander_riders = yes

		# siege weapon
		unlock_maa_anb_dwarven_bombard = yes

	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_dwarven_handgunner = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		has_cultural_pillar = heritage_dwarven
		has_cultural_era_or_later = culture_era_high_medieval
	}
	can_pick = {
		has_cultural_pillar = heritage_dwarven
		has_cultural_era_or_later = culture_era_high_medieval
	}
	
	parameters = {
		unlock_maa_anb_dwarven_handgunner = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_longbowmen = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:damerian
			any_parent_culture_or_above = {
				this = culture:damerian
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:damerian
			any_parent_culture_or_above = {
				this = culture:damerian
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_longbowmen = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

# tradition_test_maa_anb_oakfoot_rangers = {
# 	category = combat
# 	layers = {
# 		0 = martial
# 		1 = western
# 		4 = bow.dds
# 	}
	
# 	is_shown = {
# 		OR ={
# 			this = culture:bluefoot_halfling
# 			any_parent_culture_or_above = {
# 				this = culture:bluefoot_halfling
# 			}
# 			this = culture:oakfoot_halfling
# 			any_parent_culture_or_above = {
# 				this = culture:oakfoot_halfling
# 			}
# 		}
# 	}
# 	can_pick = {
# 		OR ={
# 			this = culture:bluefoot_halfling
# 			any_parent_culture_or_above = {
# 				this = culture:bluefoot_halfling
# 			}
# 			this = culture:oakfoot_halfling
# 			any_parent_culture_or_above = {
# 				this = culture:oakfoot_halfling
# 			}
# 		}
# 	}
	
# 	parameters = {
# 		unlock_maa_anb_oakfoot_rangers = yes
# 	}
	
# 	cost = {
# 		prestige = {
# 			add = {
# 				value = 1000
# 				desc = BASE
# 				format = "BASE_VALUE_FORMAT"
# 			}
# 		}
# 	}
	
# 	ai_will_do = {
# 		value = 0
# 	}
# }

# tradition_communal_defense = { 
# 	category = regional
# 	layers = {
# 		0 = martial
# 		1 = western
# 		4 = soldiers4.dds
# 	}
# 	is_shown = {
# 		has_cultural_pillar = heritage_halfling
# 	}
# 	can_pick = {
# 		has_cultural_pillar = heritage_halfling
# 	}
	
# 	parameters = {
# 		unlock_maa_anb_small_militia = yes
# 	}
# 	character_modifier = {
# 		men_at_arms_max_size_add = 1
# 	}
	
# 	cost = {
# 		prestige = {
# 			add = {
# 				value = 1000
# 				desc = BASE
# 				format = "BASE_VALUE_FORMAT"
# 			}
# 		}
# 	}
	
# 	ai_will_do = {
# 		value = 0
# 	}
# }

tradition_elk_hunters = {
	category = regional

	layers = {
		0 = martial
		1 = western
		4 = forest.dds
	}

	is_shown = {
		OR ={
			this = culture:bluefoot_halfling
			any_parent_culture_or_above = {
				this = culture:bluefoot_halfling
			}
			this = culture:oakfoot_halfling
			any_parent_culture_or_above = {
				this = culture:oakfoot_halfling
			}
		}
		NOT = { has_cultural_tradition = tradition_forest_folk }
	}
	can_pick = {
		OR ={
			this = culture:bluefoot_halfling
			any_parent_culture_or_above = {
				this = culture:bluefoot_halfling
			}
			this = culture:oakfoot_halfling
			any_parent_culture_or_above = {
				this = culture:oakfoot_halfling
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_oakfoot_rangers = yes
		logging_camps_building_bonuses = yes
		can_recruit_forest_specialist = yes
		forest_fighter_trait_more_common = yes
		forest_hunt_success_chance = yes
		watermills_forestries_unlock = yes
		hunting_archery_building_bonuses = yes
	}
	character_modifier = {
		forest_travel_danger = forest_high_danger_reduction
		taiga_travel_danger = taiga_high_danger_reduction
	}
	county_modifier = {
		forest_development_growth_factor = 0.15
		taiga_development_growth_factor = 0.15
	}
	province_modifier = {
		forest_construction_gold_cost = -0.1
		taiga_construction_gold_cost = -0.1
		forest_holding_construction_gold_cost = -0.1
		taiga_holding_construction_gold_cost = -0.1
	}

	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

# tradition_test_maa_anb_troll_hunter = {
# 	category = combat
# 	layers = {
# 		0 = martial
# 		1 = western
# 		4 = bow.dds
# 	}
	
# 	is_shown = {
# 		always = yes
# 	}
# 	can_pick = {
# 		always = yes
# 	}
	
# 	parameters = {
# 		unlock_maa_anb_troll_hunter = yes
# 	}
	
# 	cost = {
# 		prestige = {
# 			add = {
# 				value = 1000
# 				desc = BASE
# 				format = "BASE_VALUE_FORMAT"
# 			}
# 		}
# 	}
	
# 	ai_will_do = {
# 		value = 0
# 	}
# }

# tradition_test_maa_anb_forest_stalkers = {
# 	category = combat
# 	layers = {
# 		0 = martial
# 		1 = western
# 		4 = bow.dds
# 	}
	
# 	is_shown = {
# 		always = yes
# 	}
# 	can_pick = {
# 		always = yes
# 	}
	
# 	parameters = {
# 		unlock_maa_anb_forest_stalkers = yes
# 	}
	
# 	cost = {
# 		prestige = {
# 			add = {
# 				value = 1000
# 				desc = BASE
# 				format = "BASE_VALUE_FORMAT"
# 			}
# 		}
# 	}
	
# 	ai_will_do = {
# 		value = 0
# 	}
# }

tradition_test_maa_anb_arbalesters = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:vertesker
			any_parent_culture_or_above = {
				this = culture:vertesker
			}
			this = culture:crownsman
			any_parent_culture_or_above = {
				this = culture:crownsman
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:vertesker
			any_parent_culture_or_above = {
				this = culture:vertesker
			}
			this = culture:crownsman
			any_parent_culture_or_above = {
				this = culture:crownsman
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_arbalesters = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

# tradition_test_maa_anb_small_militia = {
# 	category = combat
# 	layers = {
# 		0 = martial
# 		1 = western
# 		4 = bow.dds
# 	}
	
# 	is_shown = {
# 		has_cultural_pillar = heritage_halfling
# 	}
# 	can_pick = {
# 		has_cultural_pillar = heritage_halfling
# 	}
	
# 	parameters = {
# 		unlock_maa_anb_small_militia = yes
# 	}
	
# 	cost = {
# 		prestige = {
# 			add = {
# 				value = 1000
# 				desc = BASE
# 				format = "BASE_VALUE_FORMAT"
# 			}
# 		}
# 	}
	
# 	ai_will_do = {
# 		value = 0
# 	}
# }

tradition_test_maa_anb_woodsmen = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:blue_reachman
			any_parent_culture_or_above = {
				this = culture:blue_reachman
			}
			this = culture:old_alenic
			any_parent_culture_or_above = {
				this = culture:old_alenic
			}
			this = culture:gawedi
			any_parent_culture_or_above = {
				this = culture:gawedi
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:blue_reachman
			any_parent_culture_or_above = {
				this = culture:blue_reachman
			}
			this = culture:old_alenic
			any_parent_culture_or_above = {
				this = culture:old_alenic
			}
			this = culture:gawedi
			any_parent_culture_or_above = {
				this = culture:gawedi
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_woodsmen = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_agderbedderen = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:cliff_gnomish
			any_parent_culture_or_above = {
				this = culture:cliff_gnomish
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:cliff_gnomish
			any_parent_culture_or_above = {
				this = culture:cliff_gnomish
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_agderbedderen = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_moormen_clan_raiders = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:moorman
			any_parent_culture_or_above = {
				this = culture:moorman
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:moorman
			any_parent_culture_or_above = {
				this = culture:moorman
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_moormen_clan_raiders = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_alononan = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:treocid
			any_parent_culture_or_above = {
				this = culture:treocid
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:treocid
			any_parent_culture_or_above = {
				this = culture:treocid
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_alononan = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_saerines = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:pearlsedger
			any_parent_culture_or_above = {
				this = culture:pearlsedger
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:pearlsedger
			any_parent_culture_or_above = {
				this = culture:pearlsedger
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_saerines = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_mistfighters = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:milcori
			any_parent_culture_or_above = {
				this = culture:milcori
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:milcori
			any_parent_culture_or_above = {
				this = culture:milcori
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_mistfighters = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_vernmen_veterans = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:vernid
			any_parent_culture_or_above = {
				this = culture:vernid
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:vernid
			any_parent_culture_or_above = {
				this = culture:vernid
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_vernmen_veterans = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_dercinges = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:carnetori
			any_parent_culture_or_above = {
				this = culture:carnetori
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:carnetori
			any_parent_culture_or_above = {
				this = culture:carnetori
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_dercinges = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_wexonard_greatswords = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:wexonard
			any_parent_culture_or_above = {
				this = culture:wexonard
			}
		}
		has_cultural_era_or_later = culture_era_late_medieval
	}
	can_pick = {
		OR ={
			this = culture:wexonard
			any_parent_culture_or_above = {
				this = culture:wexonard
			}
		}
		has_cultural_era_or_later = culture_era_late_medieval
	}
	
	parameters = {
		unlock_maa_anb_wexonard_greatswords = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_patrician_guards = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:castanite
			any_parent_culture_or_above = {
				this = culture:castanite
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:castanite
			any_parent_culture_or_above = {
				this = culture:castanite
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_patrician_guards = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_ebonsteel_infantry = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:olavish
			any_parent_culture_or_above = {
				this = culture:olavish
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:olavish
			any_parent_culture_or_above = {
				this = culture:olavish
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_ebonsteel_infantry = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_havoric_clansmen = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:havoric
			any_parent_culture_or_above = {
				this = culture:havoric
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:havoric
			any_parent_culture_or_above = {
				this = culture:havoric
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_havoric_clansmen = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_vrorengards = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:white_reachman
			any_parent_culture_or_above = {
				this = culture:white_reachman
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:white_reachman
			any_parent_culture_or_above = {
				this = culture:white_reachman
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_vrorengards = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_balakuran = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		this = culture:korbarid
	}
	can_pick = {
		this = culture:korbarid
	}
	
	parameters = {
		unlock_maa_anb_balakuran = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_bladesworn = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		this = culture:marcher
	}
	can_pick = {
		this = culture:marcher
	}
	
	parameters = {
		unlock_maa_anb_bladesworn = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_spadiores = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:tefori
			any_parent_culture_or_above = {
				this = culture:tefori
			}
			this = culture:old_damerian
			this = culture:lenco_damerian
			any_parent_culture_or_above = {
				this = culture:lenco_damerian
			}
			this = culture:tretunic
		}
	}
	can_pick = {
		OR ={
			this = culture:tefori
			any_parent_culture_or_above = {
				this = culture:tefori
			}
			this = culture:old_damerian
			this = culture:lenco_damerian
			any_parent_culture_or_above = {
				this = culture:lenco_damerian
			}
			this = culture:tretunic
		}
	}
	
	parameters = {
		unlock_maa_anb_spadiores = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_taldoud_guard = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:creek_gnomish
			any_parent_culture_or_above = {
				this = culture:creek_gnomish
			}
			this = culture:iochander
			any_parent_culture_or_above = {
				this = culture:iochander
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:creek_gnomish
			any_parent_culture_or_above = {
				this = culture:creek_gnomish
			}
			this = culture:iochander
		}
	}
	
	parameters = {
		unlock_maa_anb_taldoud_guard = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_arannese_bardiches = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:arannese
			any_parent_culture_or_above = {
				this = culture:arannese
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:arannese
			any_parent_culture_or_above = {
				this = culture:arannese
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_arannese_bardiches = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_mireknights = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:balmirish
			any_parent_culture_or_above = {
				this = culture:balmirish
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:balmirish
			any_parent_culture_or_above = {
				this = culture:balmirish
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_mireknights = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_isargaesos = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:sormanni
			any_parent_culture_or_above = {
				this = culture:sormanni
			}
			this = culture:sorncosti
			any_parent_culture_or_above = {
				this = culture:sorncosti
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:balmirish
			any_parent_culture_or_above = {
				this = culture:balmirish
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_isargaesos = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_bulwari_phalanx = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		has_cultural_pillar = heritage_bulwari
	}
	can_pick = {
		has_cultural_pillar = heritage_bulwari
	}
	
	parameters = {
		unlock_maa_anb_bulwari_phalanx = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_gawedi_talonmen = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		has_cultural_pillar = heritage_alenic
	}
	can_pick = {
		has_cultural_pillar = heritage_alenic
	}
	
	parameters = {
		unlock_maa_anb_gawedi_talonmen = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_thorn_squares = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		this = culture:roilsardi
		has_cultural_era_or_later = culture_era_late_medieval
	}
	can_pick = {
		this = culture:roilsardi
		has_cultural_era_or_later = culture_era_late_medieval
	}
	
	parameters = {
		unlock_maa_anb_thorn_squares = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

# tradition_test_maa_anb_femaor_warriors = {
# 	category = combat
# 	layers = {
# 		0 = martial
# 		1 = western
# 		4 = bow.dds
# 	}
	
# 	is_shown = {
# 		always = yes
# 	}
# 	can_pick = {
# 		always = yes
# 	}
	
# 	parameters = {
# 		unlock_maa_anb_femaor_warriors = yes
# 	}
	
# 	cost = {
# 		prestige = {
# 			add = {
# 				value = 1000
# 				desc = BASE
# 				format = "BASE_VALUE_FORMAT"
# 			}
# 		}
# 	}
	
# 	ai_will_do = {
# 		value = 0
# 	}
# }

tradition_test_maa_anb_burning_guard = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:ourdi
			any_parent_culture_or_above = {
				this = culture:ourdi
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:ourdi
			any_parent_culture_or_above = {
				this = culture:ourdi
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_burning_guard = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_ribbonspears = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:old_esmari
			any_parent_culture_or_above = {
				this = culture:old_esmari
			}
			this = culture:ryalani
			any_parent_culture_or_above = {
				this = culture:ryalani
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:old_esmari
			any_parent_culture_or_above = {
				this = culture:old_esmari
			}
			this = culture:ryalani
			any_parent_culture_or_above = {
				this = culture:ryalani
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_ribbonspears = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_battlemages = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		always = yes
	}
	can_pick = {
		always = yes
	}
	
	parameters = {
		unlock_maa_anb_battlemages = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_druids = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:carnetori
			any_parent_culture_or_above = {
				this = culture:carnetori
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:carnetori
			any_parent_culture_or_above = {
				this = culture:carnetori
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_druids = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_skalds = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		has_cultural_pillar = heritage_gerudian
	}
	can_pick = {
		has_cultural_pillar = heritage_gerudian
	}
	
	parameters = {
		unlock_maa_anb_skalds = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_storm_sorcerers = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:cliff_gnomish
			any_parent_culture_or_above = {
				this = culture:cliff_gnomish
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:cliff_gnomish
			any_parent_culture_or_above = {
				this = culture:cliff_gnomish
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_storm_sorcerers = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_wren_hedge_witches = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		has_cultural_pillar = heritage_halfling
	}
	can_pick = {
		has_cultural_pillar = heritage_halfling
	}
	
	parameters = {
		unlock_maa_anb_wren_hedge_witches = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_alenic_outriders = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:gawedi
			this = culture:boundsman
		}
		has_cultural_era_or_later = culture_era_early_medieval
	}
	can_pick = {
		OR ={
			this = culture:gawedi
			this = culture:boundsman
		}
		has_cultural_era_or_later = culture_era_early_medieval
	}
	
	parameters = {
		unlock_maa_anb_alenic_outriders = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_little_lords = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:roysfoot_halfling
			any_parent_culture_or_above = {
				this = culture:roysfoot_halfling
			}
		}
		NOT = { has_cultural_tradition = tradition_pastoralists }
	}
	can_pick = {
		OR ={
			this = culture:roysfoot_halfling
			any_parent_culture_or_above = {
				this = culture:roysfoot_halfling
			}
		}
	}

	parameters = {
		unlock_maa_anb_smallknights = yes
		pastures_building_bonuses = yes
		pastures_building_heavy_cav_bonus = yes
		farm_estates_pastures_unlock = yes
	}
	character_modifier = {
		negate_health_penalty_add = 0.15
		prowess = 1
		character_travel_speed_mult = 0.1
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_epuires = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		has_cultural_pillar = heritage_lencori
	}
	can_pick = {
		has_cultural_pillar = heritage_lencori
	}
	
	parameters = {
		unlock_maa_anb_epuires = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_businori_raiders = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:businori
			any_parent_culture_or_above = {
				this = culture:businori
			}
			this = culture:busilari
			any_parent_culture_or_above = {
				this = culture:busilari
			}
			this = culture:ilatani
			any_parent_culture_or_above = {
				this = culture:ilatani
			}
		}
		has_cultural_era_or_later = culture_era_early_medieval
	}
	can_pick = {
		OR ={
			this = culture:businori
			any_parent_culture_or_above = {
				this = culture:businori
			}
			this = culture:busilari
			any_parent_culture_or_above = {
				this = culture:busilari
			}
			this = culture:ilatani
			any_parent_culture_or_above = {
				this = culture:ilatani
			}
		}
		has_cultural_era_or_later = culture_era_early_medieval
	}
	
	parameters = {
		unlock_maa_anb_businori_raiders = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_wayknights = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:devacedi
			any_parent_culture_or_above = {
				this = culture:devacedi
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:devacedi
			any_parent_culture_or_above = {
				this = culture:devacedi
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_wayknights = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_knights_of_the_serpent = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:blue_reachman
			any_parent_culture_or_above = {
				this = culture:blue_reachman
			}
			this = culture:white_reachman
			any_parent_culture_or_above = {
				this = culture:white_reachman
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:blue_reachman
			any_parent_culture_or_above = {
				this = culture:blue_reachman
			}
			this = culture:white_reachman
			any_parent_culture_or_above = {
				this = culture:white_reachman
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_knights_of_the_serpent = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_rose_knights = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:lorenti
			any_parent_culture_or_above = {
				this = culture:lorenti
			}
			this = culture:entebenic
			any_parent_culture_or_above = {
				this = culture:entebenic
			}
			this = culture:crovanni
			any_parent_culture_or_above = {
				this = culture:crovanni
			}
		}
		has_cultural_era_or_later = culture_era_high_medieval
	}
	can_pick = {
		OR ={
			this = culture:lorenti
			any_parent_culture_or_above = {
				this = culture:lorenti
			}
			this = culture:entebenic
			any_parent_culture_or_above = {
				this = culture:entebenic
			}
			this = culture:crovanni
			any_parent_culture_or_above = {
				this = culture:crovanni
			}
		}
		has_cultural_era_or_later = culture_era_high_medieval
	}
	
	parameters = {
		unlock_maa_anb_rose_knights = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

# tradition_test_maa_anb_akalite_cataphracts = {
# 	category = combat
# 	layers = {
# 		0 = martial
# 		1 = western
# 		4 = bow.dds
# 	}
	
# 	is_shown = {
# 		always = yes
# 	}
# 	can_pick = {
# 		always = yes
# 	}
	
# 	parameters = {
# 		unlock_maa_anb_akalite_cataphracts = yes
# 	}
	
# 	cost = {
# 		prestige = {
# 			add = {
# 				value = 1000
# 				desc = BASE
# 				format = "BASE_VALUE_FORMAT"
# 			}
# 		}
# 	}
	
# 	ai_will_do = {
# 		value = 0
# 	}
# }

tradition_test_maa_anb_black_knights = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		this = culture:corvurian
		has_cultural_era_or_later = culture_era_high_medieval
	}
	can_pick = {
		this = culture:corvurian
		has_cultural_era_or_later = culture_era_high_medieval
	}
	
	parameters = {
		unlock_maa_anb_black_knights = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

# tradition_test_maa_anb_flying_chariots = {
# 	category = combat
# 	layers = {
# 		0 = martial
# 		1 = western
# 		4 = bow.dds
# 	}
	
# 	is_shown = {
# 		always = yes
# 	}
# 	can_pick = {
# 		always = yes
# 	}
	
# 	parameters = {
# 		unlock_maa_anb_flying_chariots = yes
# 	}
	
# 	cost = {
# 		prestige = {
# 			add = {
# 				value = 1000
# 				desc = BASE
# 				format = "BASE_VALUE_FORMAT"
# 			}
# 		}
# 	}
	
# 	ai_will_do = {
# 		value = 0
# 	}
# }

tradition_test_maa_anb_knights_of_the_spear = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:adenner
			any_parent_culture_or_above = {
				this = culture:adenner
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:adenner
			any_parent_culture_or_above = {
				this = culture:adenner
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_knights_of_the_spear = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_castellyrian_heavy_knights = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:castellyrian
			any_parent_culture_or_above = {
				this = culture:castellyrian
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:castellyrian
			any_parent_culture_or_above = {
				this = culture:castellyrian
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_castellyrian_heavy_knights = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_arbarani_heavy_riders = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:arbarani
			any_parent_culture_or_above = {
				this = culture:arbarani
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:arbarani
			any_parent_culture_or_above = {
				this = culture:arbarani
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_arbarani_heavy_riders = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_war_mammoths = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:olavish
			any_parent_culture_or_above = {
				this = culture:olavish
			}
		}
		has_cultural_era_or_later = culture_era_high_medieval
	}
	can_pick = {
		OR ={
			this = culture:olavish
			any_parent_culture_or_above = {
				this = culture:olavish
			}
		}
		has_cultural_era_or_later = culture_era_high_medieval
	}
	
	parameters = {
		unlock_maa_anb_war_mammoths = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_griffon_riders = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:marrodic
			any_parent_culture_or_above = {
				this = culture:marrodic
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:marrodic
			any_parent_culture_or_above = {
				this = culture:marrodic
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_griffon_riders = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_wyvern_riders = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:vernid
			any_parent_culture_or_above = {
				this = culture:vernid
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:vernid
			any_parent_culture_or_above = {
				this = culture:vernid
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_wyvern_riders = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_drake_riders = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:redscale
			any_parent_culture_or_above = {
				this = culture:redscale
			}
			this = culture:bluescale
			any_parent_culture_or_above = {
				this = culture:bluescale
			}
			this = culture:greenscale
			any_parent_culture_or_above = {
				this = culture:greenscale
			}
		}
		has_cultural_era_or_later = culture_era_late_medieval
	}
	can_pick = {
		OR ={
			this = culture:redscale
			any_parent_culture_or_above = {
				this = culture:redscale
			}
			this = culture:bluescale
			any_parent_culture_or_above = {
				this = culture:bluescale
			}
			this = culture:greenscale
			any_parent_culture_or_above = {
				this = culture:greenscale
			}
		}
		has_cultural_era_or_later = culture_era_late_medieval
	}
	
	parameters = {
		unlock_maa_anb_drake_riders = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}

tradition_test_maa_anb_salamander_riders = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		OR ={
			this = culture:darkscale
			any_parent_culture_or_above = {
				this = culture:darkscale
			}
		}
	}
	can_pick = {
		OR ={
			this = culture:darkscale
			any_parent_culture_or_above = {
				this = culture:darkscale
			}
		}
	}
	
	parameters = {
		unlock_maa_anb_salamander_riders = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}


tradition_test_maa_anb_dwarven_bombard = {
	category = combat
	layers = {
		0 = martial
		1 = western
		4 = bow.dds
	}
	
	is_shown = {
		has_cultural_pillar = heritage_dwarven
		has_cultural_era_or_later = culture_era_late_medieval
	}
	can_pick = {
		has_cultural_pillar = heritage_dwarven
		has_cultural_era_or_later = culture_era_late_medieval
	}
	
	parameters = {
		unlock_maa_anb_dwarven_bombard = yes
	}
	
	cost = {
		prestige = {
			add = {
				value = 1000
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
		}
	}
	
	ai_will_do = {
		value = 0
	}
}