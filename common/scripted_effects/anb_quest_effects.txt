﻿go_on_quest_in_location_effect = {
	$ADVENTURER$ = {
		set_variable = {
			name = adventure_destination
			value = $LOCATION$
		}
		
		add_character_modifier = {
			modifier = on_quest_modifier
		}
	
		create_inspiration = inspiration_$QUEST$

		# TODO - add the tooltip to say you're gonna pay
		if = {
			limit = { exists = inspiration }
			inspiration = { save_scope_as = this_inspiration }
			root = { sponsor_inspiration = scope:this_inspiration }
		}
	}
}

view_quest_interaction_effect = {
	scope:target = {
		if = {
			limit = {
				has_county_modifier = goblins_infestation_modifier
			}
			scope:actor = {
				# trigger_event = anb_goblin_quest.0001
			}
		}
	}
}

character_go_on_quest_effect_with_default_stress_impact = {
	stress_impact = {
		athletic = minor_stress_impact_loss
		craven = medium_stress_impact_gain
		lazy = medium_stress_impact_gain
		arrogant = minor_stress_impact_gain
		zealous = minor_stress_impact_loss
	}
	
	character_go_on_quest_effect = {
		ADVENTURER = $ADVENTURER$
		LOCATION = $LOCATION$
		QUEST = $QUEST$
	}
}

# Split from the above so you can make custom stress impacts. E.g ambitious would lose stress for killing a dragon, and craven may not dislike all quests!
character_go_on_quest_effect = {
	go_on_quest_in_location_effect = {
		ADVENTURER = $ADVENTURER$
		LOCATION = $LOCATION$
		QUEST = $QUEST$
	}
}

send_hired_adventurer_for_quest_effect_with_default_stress_impact = {
	stress_impact = {
		craven = minor_stress_impact_loss
		greedy = minor_stress_impact_gain
		zealous = minor_stress_impact_loss
	}

	send_hired_adventurer_for_quest_effect = {
		ADVENTURER = $ADVENTURER$
		LOCATION = $LOCATION$
		QUEST = $QUEST$
	}
}

# Split from the above so you can make custom stress impacts
send_hired_adventurer_for_quest_effect = {
	go_on_quest_in_location_effect = {
		ADVENTURER = $ADVENTURER$
		LOCATION = $LOCATION$
		QUEST = $QUEST$
	}
}

add_quest_to_county = {
	add_county_modifier = {
		modifier = $QUEST$_modifier
		days = -1
	}
}

remove_quest_from_county = {
	remove_county_modifier = $QUEST$_modifier
}

search_for_adventurers_for_quest_effect = {
	custom_tooltip = search_for_adventurers_tooltip
	trigger_event = { id = anb_$QUEST$.0002 days = { 60 120 } }
}

show_toast_for_owner_and_sponsor = {
	scope:inspiration_sponsor = {
		send_interface_toast = {
			title = $TITLE$
			left_icon = $ICON$
			custom_tooltip = $TOOLTIP$
		}
	}
	send_interface_toast = {
		title = $TITLE$
		left_icon = $ICON$
		custom_tooltip = $TOOLTIP$
	}
}

quest_duel_effect = {
	
	# ok, so, idk what is going on but
	# i think difficulty should be used to generate a random dude and then putting the new dude instead of $DIFFICULTY$
	
	
	scope:inspiration_owner = {
		duel = {
			skill = $SKILL$
			target = $DIFFICULTY$
			30 = {
				desc = $SUCCESS$

				$SUCCESS_EFFECT$

				hidden_effect = {
					show_toast_for_owner_and_sponsor = {
						TITLE = $TITLE$
						ICON = scope:inspiration_owner
						TOOLTIP = $SUCCESS$
					}
				}
				
			}
			20 = {
				desc = $FAILURE$

				$FAILURE_EFFECT$

				hidden_effect = {
					show_toast_for_owner_and_sponsor = {
						TITLE = $TITLE$
						ICON = scope:inspiration_owner
						TOOLTIP = $FAILURE$
					}
				}
			}
		}
	}
}

adventurer_remain_in_tavern_effect = {
	scope:inspiration_sponsor = {
		remove_short_term_gold = minor_gold_value
	}

	hidden_effect = {
		random_list = {
			10 = {
				modifier = {
					factor = 1.25
					scope:inspiration_owner = {
						has_trait = drunkard
					}
				}
				modifier = {
					factor = 1.5
					scope:inspiration_owner = {
						has_trait = craven
					}
				}
				# trigger_event = { id = anb_generic_quest_events.0007 days = { 30 60 } }
			}
			10 = {
				# trigger_event = { id = anb_generic_quest_events.0008 days = { 30 60 } }
			}
			80 = {}
		}
	}
}

goblin_rally_peasants_success_effect = {
	scope:quest_location = {
		add_county_modifier = {
			modifier = rallied_peasants
			days = 1825
		}
	}

	random = {
		chance = 10
		
		scope:inspiration = {
			change_inspiration_progress = 1
		}
	}
	
	scope:inspiration = {
		set_variable = rallied_peasants
	}
}