﻿#####################################
#									#
# 			Female					#
#									#
#####################################
female_eyes_normal = {
	entity = { required_tags = "kobold_pupil" 	 	entity = "female_kobold_eyes_entity"	shared_pose_entity = head }
	entity = { required_tags = ""  					entity = "female_eyes_entity"		shared_pose_entity = head }
}

female_eyes_normal_no_shadow = {
	entity = { required_tags = "kobold_pupil" 	 	entity = "female_kobold_eyes_entity"		shared_pose_entity = head }
	entity = { required_tags = ""  					entity = "female_eyes_no_shadow_entity"	shared_pose_entity = head }
}

female_eyes_normal_dark_iris = {
	entity = { required_tags = "kobold_pupil" 	 	entity = "female_kobold_eyes_entity"		shared_pose_entity = head }
	entity = { required_tags = ""  					entity = "female_eyes_dark_iris_entity"	shared_pose_entity = head }
}

female_eyes_normal_asian = {
	entity = { required_tags = "kobold_pupil" 	 	entity = "female_kobold_eyes_entity"		shared_pose_entity = head }
	entity = { required_tags = ""  					entity = "female_eyes_asian_entity"	shared_pose_entity = head }
}

female_eyes_bloodshot = {
	entity = { required_tags = "kobold_pupil" 	 	entity = "female_kobold_eyes_entity"		shared_pose_entity = head }
	entity = { required_tags = ""  					entity = "female_eyes_bloodshot_entity"	shared_pose_entity = head }
}

female_eyes_blind = {
	entity = { required_tags = "kobold_pupil" 	 	entity = "female_kobold_blind_entity"		shared_pose_entity = head }
	entity = { required_tags = ""  					entity = "female_eyes_blind_entity"		shared_pose_entity = head }
}

female_teeth_normal = {
	entity = { required_tags = "kobold_pupil" 	 	entity = "female_kobold_teeth_entity"		shared_pose_entity = head }
	entity = { required_tags = ""  					entity = "female_teeth_entity"		shared_pose_entity = head }
}

female_eyelashes_normal = {
	entity = {	entity = "female_eyelashes_entity"		 shared_pose_entity = head }
}

#####################################
#									#
# 			Male					#
#									#
#####################################

male_eyes_normal = {
	entity = { required_tags = "kobold_pupil" 	 	entity = "male_kobold_eyes_entity"	shared_pose_entity = head }
	entity = { required_tags = ""  					entity = "male_eyes_entity"		shared_pose_entity = head }
}

male_eyes_normal_no_shadow = {
	entity = { required_tags = "kobold_pupil" 	 	entity = "male_kobold_eyes_entity"		shared_pose_entity = head }
	entity = { required_tags = ""  					entity = "male_eyes_no_shadow_entity"		shared_pose_entity = head }
}

male_eyes_normal_dark_iris = {
	entity = { required_tags = "kobold_pupil" 	 	entity = "male_kobold_eyes_entity"		shared_pose_entity = head }
	entity = { required_tags = ""  					entity = "male_eyes_dark_iris_entity"		shared_pose_entity = head }
}

male_eyes_normal_asian = {
	entity = { required_tags = "kobold_pupil" 	 	entity = "male_kobold_eyes_entity"		shared_pose_entity = head }
	entity = { required_tags = ""  					entity = "male_eyes_asian_entity"		shared_pose_entity = head }
}

male_eyes_bloodshot = {
	entity = { required_tags = "kobold_pupil" 	 	entity = "male_kobold_eyes_entity"		shared_pose_entity = head }
	entity = { required_tags = ""  					entity = "male_eyes_bloodshot_entity"	shared_pose_entity = head }
}

male_eyes_blind = {
	entity = { required_tags = "kobold_pupil" 	 	entity = "male_kobold_blind_entity"		shared_pose_entity = head }
	entity = { required_tags = ""  					entity = "male_eyes_blind_entity"		shared_pose_entity = head }
}

male_teeth_normal = {
	entity = { required_tags = "kobold_pupil" 	 	entity = "male_kobold_teeth_entity"		shared_pose_entity = head }
	entity = { required_tags = ""  					entity = "male_teeth_entity"		shared_pose_entity = head }
}

male_eyelashes_normal = {
	entity = {	entity = "male_eyelashes_entity"		 shared_pose_entity = head }
}

#####################################
#									#
# 			Boy						#
#									#
#####################################

boy_eyes_normal = {
	entity = { required_tags = "kobold_pupil" 	 	entity = "male_kobold_eyes_entity"	shared_pose_entity = head }
	entity = { required_tags = ""  					entity = "male_eyes_entity"		shared_pose_entity = head }
}

boy_teeth_normal = {
	entity = { required_tags = "kobold_pupil" 	 	entity = "male_kobold_teeth_entity"		shared_pose_entity = head }
	entity = { required_tags = ""  					entity = "male_teeth_entity"		shared_pose_entity = head }
}

#####################################
#									#
# 			Girl					#
#									#
#####################################

girl_eyes_normal = {
	entity = { entity = "female_eyes_entity"		shared_pose_entity = head }
}

girl_teeth_normal = {
	entity = { required_tags = "kobold_pupil" 	 	entity = "female_kobold_teeth_entity"		shared_pose_entity = head }
	entity = { required_tags = ""  					entity = "female_teeth_entity"		shared_pose_entity = head }
}
