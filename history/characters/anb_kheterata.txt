﻿
# 3rd dynasty - Panoutid
# probably there are more before
panoutid_0001 = { # Panoute I
	name = "Panoute"
	dynasty = dynasty_panoutid
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	-23.1.1 = {
		birth = yes
	}
	
	68.1.1 = {
		death = "68.1.1"
	}
}

panoutid_0002 = { # Shenoute I
	name = "Shenoute"
	dynasty = dynasty_panoutid
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	32.1.11 = {
		birth = yes
	}
	
	105.1.1 = {
		death = "105.1.1"
	}
}

panoutid_0003 = { # Shenoute II
	name = "Shenoute"
	dynasty = dynasty_panoutid
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	91.7.23 = {
		birth = yes
	}
	
	145.1.1 = {
		death = "145.1.1"
	}
}

panoutid_0004 = { # Tanoute II
	name = "Tanoute"
	dynasty = dynasty_panoutid
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	131.2.26 = {
		birth = yes
	}
	
	154.1.1 = {
		death = "154.1.1"
	}
}

# 4th dynasty - Shenotid

shenotid_0001 = { # Shenoute III
	name = "Shenoute"
	dynasty = dynasty_shenotid
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	115.1.14 = {
		birth = yes
	}
	
	167.1.1 = {
		death = "167.1.1"
	}
}

shenotid_0002 = { # Phnouti II
	name = "Phnouti"
	dynasty = dynasty_shenotid
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	131.5.5 = {
		birth = yes
	}
	
	188.1.1 = {
		death = "188.1.1"
	}
}

shenotid_0003 = { # Persouma I
	name = "Persouma"
	dynasty = dynasty_shenotid
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	161.7.19 = {
		birth = yes
	}
	
	203.1.1 = {
		death = "203.1.1"
	}
}

shenotid_0004 = { # Shenoute IV
	name = "Shenoute"
	dynasty = dynasty_shenotid
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	186.12.21 = {
		birth = yes
	}
	
	233.1.1 = {
		death = "233.1.1"
	}
}

# 5th dynasty - Sokkanid

sokkanid_0001 = { # Sokkari I
	name = "Sokkari"
	dynasty = dynasty_sokkanid
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	202.6.5 = {
		birth = yes
	}
	
	254.1.1 = {
		death = "254.1.1"
	}
}

sokkanid_0002 = { # Piye I
	name = "Piye"
	dynasty = dynasty_sokkanid
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	234.3.11 = {
		birth = yes
	}
	
	287.1.1 = {
		death = "287.1.1"
	}
}

sokkanid_0003 = { # Taharqa I
	name = "Taharqa"
	dynasty = dynasty_sokkanid
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	256.1.29 = {
		birth = yes
	}
	
	316.1.1 = {
		death = "316.1.1"
	}
}

sokkanid_0004 = { # Sokkari II
	name = "Sokkari"
	dynasty = dynasty_sokkanid
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	298.6.12 = {
		birth = yes
	}
	
	367.1.1 = {
		death = "367.1.1"
	}
}

sokkanid_0005 = { # Taharqa II
	name = "Taharqa"
	dynasty = dynasty_sokkanid
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	351.8.27 = {
		birth = yes
	}
	
	393.1.1 = {
		death = "393.1.1"
	}
}

sokkanid_0006 = { # Shebiktu I
	name = "Shebiktu"
	dynasty = dynasty_sokkanid
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	387.5.13 = {
		birth = yes
	}
	
	463.1.1 = {
		death = "463.1.1"
	}
}

sokkanid_0007 = { # Piye II
	name = "Piye"
	dynasty = dynasty_sokkanid
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	441.2.22 = {
		birth = yes
	}
	
	475.1.1 = {
		death = "475.1.1"
	}
}

# Aakhet the Bronze

aakhet_the_bronze = {
	name = "Aakhet"
	religion = khetarchy
	culture = hapremiti
	
	# is a dragon, should be a true immortal
	
	1.1.1 = { # no idea when this dude is born, could be millenia before DoAS
		birth = yes
		#give_nickname = nick_the_bronze
	}
	
	490.1.1 = {
		death = {
			death_reason = death_disappearance
		}
	}
}

# 6th dynasty - Crodamis

crodamis_0001 = { # Hyaclos I
	name = "Hyaclos"
	dynasty = dynasty_crodamis
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	472.4.28 = {
		birth = yes
	}
	
	532.1.1 = {
		death = "532.1.1"
	}
}

crodamis_0002 = { # Erbay I
	name = "Erbay"
	dynasty = dynasty_crodamis
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	502.11.7 = {
		birth = yes
	}
	
	581.1.1 = {
		death = "581.1.1"
	}
}

crodamis_0003 = { # Bupalos I
	name = "Bupalos"
	dynasty = dynasty_crodamis
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	563.9.18 = {
		birth = yes
	}
	
	628.1.1 = {
		death = "628.1.1"
	}
}

crodamis_0004 = { # Panthes I
	name = "Panthes"
	dynasty = dynasty_crodamis
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	613.2.24 = {
		birth = yes
	}
	
	662.1.1 = {
		death = "662.1.1"
	}
}

crodamis_0005 = { # Hyaclos II
	name = "Hyaclos"
	dynasty = dynasty_crodamis
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	656.4.12 = {
		birth = yes
	}
	
	706.1.1 = {
		death = "706.1.1"
	}
}

crodamis_0006 = { # Arashk I
	name = "Arashk"
	dynasty = dynasty_crodamis
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	691.7.6 = {
		birth = yes
	}
	
	757.1.1 = {
		death = "757.1.1"
	}
}

crodamis_0007 = { # Panthes II
	name = "Panthes"
	dynasty = dynasty_crodamis
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	741.9.17 = {
		birth = yes
	}
	
	800.1.1 = {
		death = "800.1.1"
	}
}

# 7th dynasty - Crodamos

crodamos_0001 = { # Lykidas I
	name = "Lykidas"
	dynasty = dynasty_crodamos
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	782.3.16 = {
		birth = yes
	}
	
	862.1.1 = {
		death = "862.1.1"
	}
}

crodamos_0002 = { # Bupalos II
	name = "Bupalos"
	dynasty = dynasty_crodamos
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	839.8.28 = {
		birth = yes
	}
	
	904.1.1 = {
		death = "904.1.1"
	}
}

crodamos_0003 = { # Rabon I
	name = "Rabon"
	dynasty = dynasty_crodamos
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	891.4.6 = {
		birth = yes
	}
	
	949.1.1 = {
		death = "949.1.1"
	}
}

crodamos_0004 = { # Erbay II
	name = "Erbay"
	dynasty = dynasty_crodamos
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	931.2.4 = {
		birth = yes
	}
	
	1008.1.1 = {
		death = "1008.1.1"
	}
}

crodamos_0005 = { # Korydon I
	name = "Korydon"
	dynasty = dynasty_crodamos
	religion = khetarchy
	culture = hapremiti
	
	trait = race_human
	
	1003.6.21 = {
		birth = yes
	}
	
	1068.1.1 = {
		death = "1068.1.1"
	}
}

crodamos_0006 = { #DON'T DELETE ME I'M THE GRANDMA OF EBORIAN'S WIFE
	name = "Kemsaf"
	dynasty = dynasty_crodamos
	religion = khetarchy
	culture = hapremiti
	female = yes
	
	trait = race_human
	trait = education_intrigue_1
	trait = deceitful
	trait = lazy
	trait = chaste
	
	936.6.3 = {
		birth = yes
	}
	952.10.11 = {
		add_spouse = taefdares_0006
	}
	986.4.30 = {
		death = yes
	}
}