#k_akalses
##d_akalses
###c_akalses
616 = {		#Tazh-qel-Araksa

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = castle_holding

    # History
}

6327 = {		#Qistambar

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = city_holding

    # History
}

6328 = {		#Qasriklum

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = none

    # History
}

6329 = {		#Harrasipar

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = church_holding

    # History
}

2882 = {        #Arasklum

# Misc
holding = city_holding

# History

}

###c_nasilan
615 = {	    #Nasilan

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = city_holding

    # History
}

6330 = {   #Azka-Kyrus

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = castle_holding

    # History
}

6331 = {	#Ayarkumar

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = none

    # History
}

###c_uqlum
617 = {		#Uqlum

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = castle_holding

    # History
}

6353 = {		#Faribar

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = city_holding

    # History
}

6354 = {		#Ilagka

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = none

    # History
}

##d_azkabar
###c_azkabar
613 = {		#Azkabar

    # Misc
    culture = zanite
    religion = cult_of_the_law
	holding = castle_holding

    # History
}

6335 = {		#Eduz-Attalu

    # Misc
    culture = zanite
    religion = cult_of_the_law
	holding = church_holding

    # History
}

6336 = {		#Dayyankalis

    # Misc
    culture = zanite
    religion = cult_of_the_law
	holding = city_holding

    # History
}

6611 = {		#Rostamdrus

    # Misc
    culture = zanite
    religion = cult_of_the_law
	holding = none

    # History
}

6612 = {		#Surasan

    # Misc
    culture = zanite
    religion = cult_of_the_law
	holding = city_holding

    # History
}

###c_kuokrumar
628 = {		#Kuokrumar

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
	holding = castle_holding

    # History
}

6337 = {		#Nohltus

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
	holding = none

    # History
}

6338= {		#Zanbiklum

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
	holding = city_holding

    # History
}

###c_markumar
624 = {		#Markumar

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
	holding = castle_holding

    # History
}

6339 = {		#Dittugalm

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
	holding = church_holding

    # History
}

6340 = {		#Isitkast

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
	holding = none
    # History
}

##d_setadazar
###c_setadazar
622 = {		#Setadazar

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = castle_holding

    # History
}

6332 = {		#Alaknis

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = city_holding

    # History
}

6333 = {		#Zanakal

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = none

    # History
}

6334 = {		#Rakanza

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = city_holding

    # History
}

###c_danas_aban
621 = {		#Danas Aban

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = castle_holding

    # History
}

6341 = {		#Iduzbar

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = city_holding
    # History
}

6342 = {		#Eburumar

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = none
    # History
}

###c_orean
623 = {		#Orean

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
	holding = castle_holding

    # History
}

6350 = {		#Anziwan

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
	holding = none
    # History
}

6351 = {		#Zagneser

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
	holding = church_holding
    # History
}

6352 = {		#Esdasma

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
	holding = city_holding
    # History
}

##d_kalisad
###c_kalisad
618 = {		#Kalisad

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = castle_holding

    # History
}

6345 = {		#Eduz-tibad

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = church_holding
    # History
}

6346 = {		#Azkarexan

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = none
    # History
}

###c_mitiq
620 = {		#Mitiq

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = castle_holding

    # History
}

6343 = {		#Lamassar

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = church_holding
    # History
}

6344 = {		#Subar

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = none
    # History
}

619 = {		#Barzilhar

    # Misc
    culture = akalsesi
    religion = way_of_kings
	holding = city_holding

    # History
}

##d_avamezan
###c_erubas
632 = {		#Erubas

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
    holding = castle_holding
    # History
}

6620 = {		#

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
    holding = none
    # History
}

6621 = {		#

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
    holding = none
    # History
}

###c_dazar_avamezan
633 = {		#Panuzan

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
    holding = castle_holding
    # History
}

6622 = {		#

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
    holding = none
    # History
}

6623 = {		#

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
    holding = none
    # History
}

6624 = {		#

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
    holding = none
    # History
}

###c_kutiriq
630 = {		#Kutiriq

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
    holding = castle_holding
    # History
}

6616 = {		#

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
    holding = none
    # History
}

6617 = {		#

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
    holding = none
    # History
}

6618 = {		#

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
    holding = none
    # History
}

6619 = {		#

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
    holding = none
    # History
}

###c_nerdu_avamezan
629 = {		#Nerdu Avamezan

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
    holding = castle_holding
    # History
}

6613 = {		#

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
    holding = none
    # History
}

6614 = {		#

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
    holding = none
    # History
}

6615 = {		#

    # Misc
    culture = akalsesi
    religion = cult_of_the_law
    holding = none
    # History
}