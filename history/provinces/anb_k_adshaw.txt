#k_adshaw
##d_adshaw
###c_rycastle
2679 = {		#Rycastle

    # Misc
    culture = old_alenic
    religion = cult_of_falah
	holding = castle_holding

    # History
}
731 = {

    # Misc
    holding = city_holding

    # History

}

2678 = {

    # Misc
    holding = none

    # History

}
###c_dinesck
752 = {		#Dinesck

    # Misc
    culture = old_alenic
    religion = cult_of_falah
	holding = castle_holding

    # History
}
2680 = {

    # Misc
    holding = none

    # History

}
2681 = {

    # Misc
    holding = city_holding

    # History

}

###c_north_greatwood
702 = {		#Shawmark

    # Misc
    culture = old_alenic
    religion = cult_of_falah
	holding = castle_holding

    # History
}
2677 = {

    # Misc
    holding = none

    # History

}

##d_bayvic
###c_bayvic
723 = {		#Bayvic

    # Misc
    culture = blue_reachman
    religion = cult_of_falah
	holding = city_holding

    # History
}
705 = {		#Ordal

    # Misc
	holding = city_holding

    # History
}
2682 = {

    # Misc
    holding = none

    # History

}
2683 = {

    # Misc
    holding = castle_holding

    # History

}
2684 = {

    # Misc
    holding = none

    # History

}
2685 = {

    # Misc
    holding = none

    # History

}
2686 = {

    # Misc
    holding = church_holding

    # History

}

##d_west_chillsbay
###c_alencay
721 = {		#Alencay

    # Misc
    culture = blue_reachman
    religion = skaldhyrric_faith
	holding = castle_holding

    # History
}
2691 = {

    # Misc
    holding = none

    # History

}
2692 = {

    # Misc
    holding = city_holding

    # History

}
2693 = {

    # Misc
    holding = none

    # History

}

###c_frostwall
706 = {		#Frostwall

    # Misc
    culture = old_alenic
    religion = cult_of_falah
	holding = castle_holding

    # History
}
2690 = {

    # Misc
    holding = none

    # History

}

###c_everwharf
722 = {		#Everwharf

    # Misc
    culture = blue_reachman
    religion = cult_of_falah
	holding = castle_holding

    # History
}
2689 = {

    # Misc
    holding = none

    # History

}

###c_reachspier
704 = {		#Reachspier

    # Misc
    culture = blue_reachman
    religion = cult_of_falah
	holding = castle_holding

    # History
}
2687 = {

    # Misc
    holding = church_holding

    # History

}
2688 = {

    # Misc
    holding = none

    # History

}

##d_adderwood
###c_adderwood
703 = {		#Adderwood

    # Misc
    culture = old_alenic
    religion = cult_of_falah
	holding = castle_holding

    # History
}
2675 = {

    # Misc
    holding = none

    # History

}
2676 = {

    # Misc
    holding = none

    # History

}

###c_coldmarket
700 = {		#Coldmarket

    # Misc
    culture = blue_reachman
    religion = skaldhyrric_faith
	holding = castle_holding

    # History
}
2670 = {

    # Misc
    holding = city_holding

    # History

}
2671 = {

    # Misc
    holding = none

    # History

}

###c_envermarck
724 = {		#Envermarck

    # Misc
    culture = blue_reachman
    religion = skaldhyrric_faith
	holding = castle_holding

    # History
}
2672 = {

    # Misc
    holding = none

    # History

}
2673 = {

    # Misc
    holding = none

    # History

}
2674 = {

    # Misc
    holding = city_holding

    # History

}

##d_serpentgard
###c_serpentback
2663 = {		#Serpentback

    # Misc
    culture = blue_reachman
    religion = cult_of_falah
	holding = castle_holding

    # History
}
699 = {

    # Misc
    holding = church_holding

    # History

}
2664 = {

    # Misc
    holding = none

    # History

}

###c_eyegard
2659 = {		#Eyegard

    # Misc
    culture = blue_reachman
    religion = cult_of_falah
	holding = castle_holding

    # History
}
698 = {

    # Misc
    holding = church_holding

    # History

}
2660 = {

    # Misc
    holding = none

    # History

}

###c_mawdock
697 = {		#Mawdock

    # Misc
    culture = blue_reachman
    religion = cult_of_falah
	holding = castle_holding

    # History
}
2661 = {

    # Misc
    holding = none

    # History

}
2662 = {

    # Misc
    holding = city_holding

    # History

}

##d_celmaldor
###c_celmaldor
696 = {		#Celmaldor

    # Misc
    culture = moon_elvish
    religion = elven_forebears
	holding = city_holding

    # History
}
2665 = {

    # Misc
    holding = church_holding

    # History

}
2666 = {

    # Misc
    holding = city_holding

    # History

}
2667 = {

    # Misc
    holding = none

    # History

}
2668 = {

    # Misc
    holding = none

    # History

}

###c_deland
695 = {		#Deland

    # Misc
    culture = blue_reachman
    religion = cult_of_falah
	holding = castle_holding

    # History
}
2669 = {

    # Misc
    holding = none

    # History

}
