#k_azka_sur
##d_azka_sur
###c_azka_sur
643 = {		#Azka-Sur

    # Misc
    culture = surani
    religion = rite_of_hammura
	holding = castle_holding

    # History
}

6669 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6670 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6671 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6672 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

###c_sardazar
642 = {		#Sardazar

    # Misc
    culture = surani
    religion = rite_of_hammura
	holding = castle_holding

    # History
}

6666 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6667 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6668 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

###c_saranaz
644 = {		#Saranaz

    # Misc
    culture = surani
    religion = rite_of_hammura
	holding = castle_holding

    # History
}

6661 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6662 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6663 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

###c_sarsad
647 = {		#SarSad

    # Misc
    culture = surani
    religion = rite_of_hammura
	holding = castle_holding

    # History
}

6664 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6665 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

##d_jikarzax
###c_jikarzax
585 = {		#Jikarzax

    # Misc
    culture = masnsih
    religion = rite_of_hammura
	holding = tribal_holding

    # History
}

6659 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6660 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

###c_zanagu
558 = {		#Zanagu

    # Misc
    culture = masnsih
    religion = rite_of_hammura
	holding = tribal_holding

    # History
}

6657 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6658 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

##d_nabilsu
###c_nabilsu
645 = {		#Nabilsu

    # Misc
    culture = surani
    religion = rite_of_hammura
	holding = castle_holding

    # History
}

6653 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6654 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

###c_irrkornaz
648 = {		#Irrkornaz

    # Misc
    culture = surani
    religion = rite_of_hammura
	holding = castle_holding

    # History
}

6651 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6652 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

###c_xharnax
646 = {

    # Misc
    culture = surani
    religion = rite_of_hammura
	holding = castle_holding

    # History
}

6655 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6656 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}