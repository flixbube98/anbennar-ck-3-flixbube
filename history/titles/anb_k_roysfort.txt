k_roysfort = {
	1000.1.1 = { change_development_level = 8 }
	990.4.5 = {
		holder = roysfort_0003 #Barton Roysfort
	}
}

d_roysfort = {
	990.4.5 = {
		holder = roysfort_0003 #Barton Roysfort
	}
}

c_roysfort = {
	990.4.5 = {
		holder = roysfort_0003 #Barton Roysfort
	}
}

c_smallmarches = {
	1002.1.1 = {
		holder = marchfoot_0001 # Lord Awstyn Marchfoot
	}
	1016.1.1 = {
		holder = marchfoot_0002 # Lord Anton Marchfoot
	}
	1022.1.1 = {
		liege = d_roysfort
	}
}

c_greenberry = {
	990.4.5 = {
		holder = roysfort_0003 #Barton Roysfort
	}
}

d_bigwheat = {
	960.1.1 = {
		holder = wheatling_0001 # Lord Galin wheatling
	}
	975.1.1 = {
		holder = wheatling_0002 # Lord Dustin wheatling
	}
	1013.1.1 = {
		holder = wheatling_0003 # Lord Kelgo wheatling
	}
	1022.1.1 = {
		liege = k_roysfort
	}
}

c_bigwheat = {
	960.1.1 = {
		holder = wheatling_0001 # Lord Galin wheatling
	}
	975.1.1 = {
		holder = wheatling_0002 # Lord Dustin wheatling
	}
	1013.1.1 = {
		holder = wheatling_0003 # Lord Kelgo wheatling
	}
}

c_middlewood = {
	1000.1.1 = { change_development_level = 7 }
	960.1.1 = {
		holder = wheatling_0001 # Lord Galin wheatling
		liege = d_bigwheat
	}
	975.1.1 = {
		holder = wheatling_0004 # Lord Robin wheatling
	}
	1021.1.1 = {
		holder = wheatling_0005 # Lady Minnie wheatling, Countess of Middlewood
	}
}