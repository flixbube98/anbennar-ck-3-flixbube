k_ourdia = {
	1000.1.1 = { change_development_level = 8 }
	1012.11.20 = {
		holder = aldwigard0001
		liege = e_bulwar
	}
}

d_tencmarck = {
	1012.11.20 = {
		holder = aldwigard0001
	}
}

d_lencmarck = {
	1000.1.1 = { change_development_level = 7 }
}

c_bal_ouord = {
	1000.1.1 = { change_development_level = 10 }
	1012.11.20 = {
		holder = aldwigard0001
	}
}

c_aldwigard = {
	1000.1.1 = { change_development_level = 7 }
	997.3.12 = {
		holder = aldwigard0001
	}
}

c_humacs_island = {
	994.3.6 = {
		holder = orstengard_0009
	}
	1012.11.20 = {
		liege = k_ourdia
	}
}

d_oudmarck = {
	1000.1.1 = { change_development_level = 7 }
}

c_dostanesck = {
	1021.6.7 = {
		holder = dostanesck0001
		liege = k_ourdia
	}
}

c_ueleden = {
	1021.6.7 = {
		holder = ueleden0001
		liege = k_ourdia
	}
}

c_demonsfall = {
	1021.6.7 = {
		holder = demonsfall0001
		liege = k_ourdia
	}
}