k_ciderfield = {
	1000.1.1 = {
		change_development_level = 8
	}
	420.1.1 = {
		holder = pearmain_0001 # King Pomard Pearmain
	}
}

d_pearview = {
	1000.1.1 = {
		holder = peartree_0002 # Lord Frederic Peartree
		
	}
}

c_butterburn = {
	1000.1.1 = {
		change_development_level = 10
	}
	1000.1.1 = {
		holder = butters_0001 # Lord Norman “the Butterball” Butters
		liege = d_pearview
	}
}

c_pearview = {
	1000.1.1 = {
		holder = peartree_0002 # Lord Frederic Peartree
	}
}

c_newcobble = {
	490.1.1 = {
		holder = pearmain_0001 # King Pomard Pearmain
	}
	1000.1.1 = {
		holder = peartree_0002 # Lord Frederic Peartree
	}
}

d_appleton = {
	1000.1.1 = {
		holder = appleseed_0004 # Lord Jon Appleseed
	}
}

c_appleton = {
	1000.1.1 = {
		holder = appleseed_0004 # Lord Jon Appleseed
	}
}

c_cowskeep = {
	995.1.1 = {
		holder = cowkeeper_0002 # Lord Willam “the Auroch” Cowkeeper
	}
	1000.1.1 = {
		liege = d_appleton
	}
}

c_littlebrook = {
	1000.1.1 = {
		holder = appleseed_0004 # Lord Jon Appleseed
	}
}