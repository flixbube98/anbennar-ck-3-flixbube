k_firanyalen = {
	1018.3.5 = { holder = heunthulyra001 }
}

c_vainviria = {
	1000.1.1 = { change_development_level = 18 }
}

c_nansalen = {
	1000.1.1 = { change_development_level = 12 }
}

c_khaajesfo = {
	1000.1.1 = { change_development_level = 10 }
}

c_mionfo = {
	1000.1.1 = { change_development_level = 12 }
}

c_misarallen = {
	1000.1.1 = { change_development_level = 14 }
}

c_mynaurame = {
	1000.1.1 = { change_development_level = 12 }
}

c_qarvetarqan = {
	1000.1.1 = { change_development_level = 13 }
}

c_kotierontol = {
	1000.1.1 = { change_development_level = 13 }
}

c_firanyalen = { 
	1000.1.1 = { change_development_level = 13 }
}

c_ijenmyfor = {
	1000.1.1 = { change_development_level = 13 }
}