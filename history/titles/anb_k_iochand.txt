k_iochand = {
	1000.1.1 = { change_development_level = 8 }
	471.1.1 = {
		holder = iochand_0102 #Fizwick Allspark
	}
	832.1.1 = {
		holder = iochand_0101 #Schleemo Iochand
	}
	962.11.14 = {
		holder = iochand_0001 #Carwick Iochand
	}
}

c_iochand = {
	962.11.13 = {
		holder = iochand_0001 #Carwick Iochand
	}
}

c_eldergreen = {
	962.11.13 = {
		holder = iochand_0001 #Carwick Iochand
	}
}

c_carverhold = {
	471.1.1 = {
		liege = k_iochand
	}
	990.2.3 = {
		holder = bluddythol_0001 #Maric Bluddythol
	}
}

d_portnamm = {
	471.1.1 = {
		liege = k_iochand
	}
	1015.6.1 = {
		government = republic_government
		holder = timekeeper_0001 #Royan Timekeeper
	}
}

c_portnamm = {
	1000.1.1 = { change_development_level = 15 }
	1015.6.1 = {
		government = republic_government
		holder = timekeeper_0001 #Royan Timekeeper
	}
}

d_southroy = {
	471.1.1 = {
		liege = k_iochand
		holder = southroy_0102 #Erna Tonks
	}
	912.11.19 = {
		holder = southroy_0101 #Torwick Southroy
	}
	996.4.23 = {
		holder = southroy_0001 #Mindi Southroy
	}
}

c_southroy = {
	996.4.23 = {
		holder = southroy_0001 #Mindi Southroy
	}
}

c_sildthol = {
	996.4.23 = {
		holder = southroy_0001 #Mindi Southroy
	}
}

c_thunderward = {
	471.1.1 = {
		liege = d_southroy
	}
	1000.1.1 = { change_development_level = 7 }
	970.1.1 = { # placeholder because someone didn't give this dude any date
		holder = donnderward_0101
	}
	990.2.3 = {
		holder = donnderward_0001 #Cecil Donnderward
	}
}

c_haysfield = {
	471.1.1 = {
		liege = d_southroy
	}
	980.1.1 = { # placeholder because someone didn't give this dude any date
		holder = hooifield_0101
	}
	1019.8.30 = {
		holder = hooifield_0001 #Robin Hooifield
	}
}