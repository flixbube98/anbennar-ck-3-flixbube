k_ginerdu = {
	1006.6.22 = {
		liege = e_bulwar
		holder = sun_elvish0034	#Veharia
	}
}

c_ginerdu = {
	1000.1.1 = { change_development_level = 8 }
	1006.6.22 = {
		liege = k_ginerdu
		holder = sun_elvish0034 # Veharia Lezuir
	}
}

c_irrliam = {
	1000.1.1 = { change_development_level = 11 }
	1006.6.22 = {
		liege = k_ginerdu
		holder = sun_elvish0028 # Irrlion Irrliazuir
	}
}

c_medurubar = {
	1000.1.1 = { change_development_level = 20 }
}

c_aklum = {
	1000.1.1 = { change_development_level = 12 }
}

c_anzabad = {
	1000.1.1 = { change_development_level = 18 }
}

c_zanakes = {
	1000.1.1 = { change_development_level = 8 }
}

c_duklum_tanuz = {
	1000.1.1 = { change_development_level = 7 }
}

c_idanas = {
	1000.1.1 = { change_development_level = 8 }
}

c_asr_limes = {
	1000.1.1 = { change_development_level = 9 }
}